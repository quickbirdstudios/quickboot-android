buildscript {

    repositories {
        google()
        mavenCentral()
        jcenter()
    }

    dependencies {
        classpath(Deps.Plugins.android)
        classpath(Deps.Plugins.kotlin)
        classpath(Deps.Plugins.bintray)
    }
}

allprojects {
    repositories {
        google()
        mavenCentral()
        jcenter()
    }
}
