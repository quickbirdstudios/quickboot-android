package com.quickbirdstudios.quickboot.test

import com.quickbirdstudios.quickboot.Quick
import org.junit.Assert.assertTrue
import org.junit.Before
import org.junit.Test
import org.kodein.di.direct
import org.kodein.di.generic.bind
import org.kodein.di.generic.instance
import org.kodein.di.generic.provider
import org.kodein.di.generic.singleton


class OverrideBindingsTest {

    abstract class SingletonDependency {
        class A : SingletonDependency()
        class B : SingletonDependency()
    }

    abstract class ProviderDependency {
        class C : ProviderDependency()
        class D : ProviderDependency()
    }


    @Before
    fun setup() {
        Quick.setBindings {
            bind<SingletonDependency>() with singleton {
                SingletonDependency.A()
            }

            bind<ProviderDependency>() with provider {
                ProviderDependency.C()
            }
        }
    }

    @Test
    fun override_singletonDependency() {
        assertTrue(Quick.direct.instance<SingletonDependency>() is SingletonDependency.A)
        Quick.overrideBindings {
            bind<SingletonDependency>() with singleton {
                SingletonDependency.B()
            }
        }
        assertTrue(Quick.direct.instance<SingletonDependency>() is SingletonDependency.B)
    }

    @Test
    fun override_providerDependency() {
        assertTrue(Quick.direct.instance<ProviderDependency>() is ProviderDependency.C)
        Quick.overrideBindings {
            bind<ProviderDependency>() with provider {
                ProviderDependency.D()
            }
        }
        assertTrue(Quick.direct.instance<ProviderDependency>() is ProviderDependency.D)
    }

}