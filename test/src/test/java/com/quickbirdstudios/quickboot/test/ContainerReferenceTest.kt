package com.quickbirdstudios.quickboot.test

import com.quickbirdstudios.quickboot.Quick
import org.junit.Assert.assertSame
import org.junit.Assert.assertTrue
import org.junit.Before
import org.junit.Test
import org.kodein.di.Kodein

class ContainerReferenceTest {

    @Before
    fun setup() {
        Quick.setBindings { }
    }

    @Test
    fun get() {
        val container = Quick.kodeinReference().get()
        assertTrue(container is Kodein)
    }

    @Test
    fun set() {
        val kodein = Kodein {}
        val reference = Quick.kodeinReference()
        reference.set(kodein)

        assertSame(kodein, Quick.kodeinReference().get())
    }

}