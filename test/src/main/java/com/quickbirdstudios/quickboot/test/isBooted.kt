package com.quickbirdstudios.quickboot.test

import com.quickbirdstudios.quickboot.Quick


//region Public API

/**
 * `true`, if [Quick.boot] was previously called
 */
val Quick.isBooted: Boolean
    get() {
        return kodeinReference().get() != null
    }

//endregion