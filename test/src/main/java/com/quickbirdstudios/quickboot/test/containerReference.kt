package com.quickbirdstudios.quickboot.test

import com.quickbirdstudios.quickboot.Quick
import org.kodein.di.Kodein
import java.util.concurrent.atomic.AtomicReference

//region Public API

internal fun Quick.kodeinReference(): AtomicReference<Kodein> {
    val field = this.javaClass.getDeclaredField("kodeinReference")
    field.isAccessible = true
    try {
        @Suppress("UNCHECKED_CAST")
        return field.get(Quick) as AtomicReference<Kodein>
    } finally {
        field.isAccessible = false
    }
}

//endregion