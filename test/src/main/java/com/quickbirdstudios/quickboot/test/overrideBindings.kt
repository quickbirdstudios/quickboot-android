package com.quickbirdstudios.quickboot.test

import com.quickbirdstudios.quickboot.Quick
import org.kodein.di.Kodein

//region Public API

/**
 * Will override current bindings held by the `Quick` object.
 * Subsequent dependency retrievals of `Quick` will use the overridden bindings.
 *
 * In order to override the bindings, a new `Kodein` container will be created, extending the current bindings.
 *
 * @param allowSilentOverride: If set to false, new bindings have to explicitly declare that they `override` an
 * existing binding
 */
fun Quick.overrideBindings(allowSilentOverride: Boolean = true, init: Kodein.MainBuilder.() -> Unit) {
    val newContainer = Kodein(allowSilentOverride = allowSilentOverride) {
        extend(Quick)
        init()
    }

    kodeinReference().set(newContainer)
}

//endregion


