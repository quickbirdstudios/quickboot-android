plugins {
    id("com.android.library")
    id("com.jfrog.bintray")
    kotlin("android")
    `maven-publish`
}

android {
    compileSdkVersion(Project.Android.compileSdkVersion)

    defaultConfig {
        minSdkVersion(Project.Android.minSdkVersion)
        targetSdkVersion(Project.Android.targetSdkVersion)
        testInstrumentationRunner = Project.Android.testInstrumentationRunner
    }
}

dependencies {
    /* android */
    implementation(Deps.Android.appCompat)
    api(Deps.Android.lifecycleExtensions)

    /* kotlin */
    implementation(Deps.Kotlin.stdLib)

    /* test */
    testImplementation(Deps.Test.junit)
    androidTestImplementation(Deps.Test.androidTestRunner)
    androidTestImplementation(Deps.Test.espresso)
    androidTestImplementation(Deps.Test.rules)

    /* Third party libraries */
    api(Deps.kodein)
}


//region Publishing

val sourcesJar by tasks.registering(Jar::class) {
    this.classifier = "sources"
    from(android.sourceSets.get("main").java.srcDirs)
}

publishing {
    publications {
        create<MavenPublication>("aar") {
            groupId = Library.groupId
            artifactId = Library.Name.lib
            version = Library.version

            artifact(file("$buildDir/outputs/aar/lib-release.aar"))
            artifact(sourcesJar.get())

            pom.withXml {
                asNode().appendNode("dependencies").apply {
                    configurations.api.get().dependencies.forEach {
                        val dependencyNode = appendNode("dependency")
                        dependencyNode.appendNode("groupId", it.group)
                        dependencyNode.appendNode("artifactId", it.name)
                        dependencyNode.appendNode("version", it.version)
                    }
                }
            }
        }
    }
}


bintray {
    user = project.properties.getOrDefault("bintray_user", "stub").toString()
    key = project.properties.getOrDefault("bintray_apikey", "stub").toString()
    setPublications("aar")
    with(pkg) {
        name = Library.Name.lib
        repo = Library.Bintray.repository
        userOrg = Library.Bintray.organization
        desc = Library.Meta.description
        websiteUrl = Library.Meta.websiteUrl
        vcsUrl = Library.Meta.gitUrl
        setLicenses(*Library.Bintray.allLicenses)
        publish = true
        with(version) {
            name = Library.version
            desc = Library.version
            with(gpg) {
                sign = false
                passphrase == project.properties.getOrDefault("bintray_gpg_password", "stub").toString()
            }
        }
    }
}

//endregion