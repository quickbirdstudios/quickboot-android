@file:Suppress("unused")

package com.quickbirdstudios.quickboot

import com.quickbirdstudios.quickboot.architecture.QuickApplication
import com.quickbirdstudios.quickboot.di.ViewModelFactory
import com.quickbirdstudios.quickboot.exception.QuickBootException
import org.kodein.di.Kodein
import org.kodein.di.KodeinContainer
import org.kodein.di.generic.bind
import org.kodein.di.generic.provider
import java.util.concurrent.atomic.AtomicReference


object Quick : Kodein {

    //region API

    override val container: KodeinContainer get() = container()

    fun boot(application: QuickApplication) = performBoot(application)

    fun configure(): QuickConfiguration = QuickConfiguration

    //endregion


    //region Private API: Container referencing


    private val kodeinReference = AtomicReference<Kodein>()

    private fun container(): KodeinContainer {
        val kodein = kodeinReference.get() ?: throw missingContainerException()
        return kodein.container
    }

    private fun missingContainerException() = QuickBootException(
        "Missing dependency container. Quick.boot not called previously!"
    )

    //endregion


    //region Private API: Booting

    private fun performBoot(application: QuickApplication) {
        throwIfAlreadyBooted()
        this.kodeinReference.set(createKodein(application))
    }

    private fun throwIfAlreadyBooted() {
        if (kodeinReference.get() != null) {
            throw QuickBootException(
                "Quick.boot cannot be called twice"
            )
        }
    }

    private fun createKodein(application: QuickApplication) = Kodein {
        extend(application.dependencies())
        bind<QuickApplication>() with provider { application }
        bind<ViewModelFactory>() with provider { ViewModelFactory(Quick) }
    }

    //endregion

}