@file:Suppress("unused")

package com.quickbirdstudios.quickboot.architecture

/**
 * Created by sebastiansellmair on 13.02.18.
 */


@Suppress("UNCHECKED_CAST")
interface QuickViewModel<out Input, out Output> {
    val input: Input
    val output: Output
}







