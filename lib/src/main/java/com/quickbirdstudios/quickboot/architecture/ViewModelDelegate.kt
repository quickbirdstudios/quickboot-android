package com.quickbirdstudios.quickboot.architecture

import android.arch.lifecycle.GenericLifecycleObserver
import android.arch.lifecycle.Lifecycle
import android.arch.lifecycle.LifecycleOwner
import kotlin.properties.ReadOnlyProperty
import kotlin.reflect.KProperty

//region Internal API

@PublishedApi
internal fun <T> LifecycleOwner.lazyViewModel(factory: () -> T): Lazy<T> {
    return ViewModelDelegate(lifecycle, factory)
}

/**
 * Lazy implementation that will automatically initialize once the corresponding lifecycle
 * reaches [Lifecycle.Event.ON_CREATE]
 */
@PublishedApi
internal class ViewModelDelegate<T>(
    private val lifecycle: Lifecycle,
    factory: () -> T
) : ReadOnlyProperty<Any?, T>, Lazy<T> {


    //region API: Implementation of `ReadOnlyProperty

    override fun getValue(thisRef: Any?, property: KProperty<*>): T {
        return viewModel.getValue(thisRef, property)
    }

    //endregion

    //region API: Implementation of `Lazy<T>`

    override val value: T get() = viewModel.value

    override fun isInitialized(): Boolean = viewModel.isInitialized()

    //endregion


    //region Private API

    private val viewModel: Lazy<T> = lazy(factory)

    private val observer = object : GenericLifecycleObserver {
        override fun onStateChanged(source: LifecycleOwner?, event: Lifecycle.Event?) {
            if (event == Lifecycle.Event.ON_CREATE) {
                lifecycle.removeObserver(this)

                /* Access `viewModel.value` to ensure initialization of `viewModel` */
                viewModel.value
            }
        }
    }

    init {
        lifecycle.addObserver(observer)
    }


    //endregion

}

//endregion


