package com.quickbirdstudios.quickboot.architecture

import org.kodein.di.Kodein


/**
 * Created by sebastiansellmair on 13.02.18.
 */
interface QuickApplication {
    fun dependencies(): Kodein = Kodein { }
}