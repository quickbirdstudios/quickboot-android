@file:Suppress("unused")

package com.quickbirdstudios.quickboot.di

import android.arch.lifecycle.ViewModel
import android.arch.lifecycle.ViewModelProvider
import org.kodein.di.Kodein
import org.kodein.di.TT
import org.kodein.di.TypeToken
import org.kodein.di.direct

/*
################################################################################################
PUBLIC API
################################################################################################
*/

class ViewModelFactory(private val kodein: Kodein) : ViewModelProvider.Factory {
    override fun <T : ViewModel> create(modelClass: Class<T>): T {
        val token: TypeToken<T> = TT(modelClass)
        return kodein.direct.Instance(token)
    }
}

