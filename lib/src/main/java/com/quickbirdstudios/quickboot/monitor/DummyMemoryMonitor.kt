package com.quickbirdstudios.quickboot.monitor

import android.arch.lifecycle.LifecycleOwner
import android.util.Log

internal class DummyMemoryMonitor : MemoryMonitor {
    override fun start() = Unit
    override fun stop() = Unit

    companion object {
        const val LOG_TAG = "QuickBoot/MemoryMonitor"
    }

    override fun monitor(lifecycleOwner: LifecycleOwner) {
        Log.d(LOG_TAG, "DummyMemoryMonitor will not track $lifecycleOwner")
    }

}