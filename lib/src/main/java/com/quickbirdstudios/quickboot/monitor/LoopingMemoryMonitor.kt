package com.quickbirdstudios.quickboot.monitor

import android.app.Activity
import android.app.Fragment
import android.arch.lifecycle.Lifecycle
import android.arch.lifecycle.LifecycleOwner
import android.util.Log

/**
 * # The standard implementation of [MemoryMonitor]
 * This implementation will check inside a loop if objects are still in memory, even if there
 * lifecycle is in state [Lifecycle.State.DESTROYED]. A message will be printed to the log, every
 * time such a potential leak is found. This means, that a leaked [Activity] or [Fragment] will
 * lead to a reoccurring error message.
 *
 * ## Implementation
 * A weak reference to the objects is held, while a background thread checks the instances
 * in a specific time interval.
 *
 */
internal class LoopingMemoryMonitor : MemoryMonitor {

    companion object {
        const val SLEEP_TIME = 1000L
        const val GC_PRESSURE_SIZE = 256
        const val THREAD_NAME = "Memory Monitor Thread"
        const val LOG_TAG = "QuickBoot/MemoryMonitor"
        const val LOG_WARNING = "Potential memory leak detected for object"
    }


    private val monitorThread = Thread(this::loop)

    /**
     * All references to objects are held weakly
     */
    private val references = mutableListOf<MonitorReference>()

    /**
     * A "probe" object for GC collection.
     */
    private var gcProbe: GCProbe = GCProbe(0)

    private val stopFlagMonitor = Any()
    private var stopFlag = false


    /**
     * See [LoopingMemoryMonitor]
     *
     * This method is idempotent. Calling it multiple times with the same [LifecycleOwner]
     * is no problem.
     */
    override fun monitor(lifecycleOwner: LifecycleOwner) {
        synchronized(references) {
            references.removeAll { it.get() == lifecycleOwner }
            references.add(MonitorReference(lifecycleOwner))
        }
    }

    private fun check() {
        synchronized(references) {
            references.removeAll { it.get() == null }

            if (didCollectGarbage()) {
                Log.d(LOG_TAG, "Detected GC event...")
                references.forEach {
                    val obj = it.get() ?: return@forEach

                    if (it.potentialLeak) {
                        Log.e(LOG_TAG, "$LOG_WARNING $obj")
                    }
                }
            }
        }
    }

    private fun loop() {
        gcProbe = GCProbe(GC_PRESSURE_SIZE)

        while (true) {
            check()
            Thread.sleep(SLEEP_TIME)
        }
    }


    private fun didCollectGarbage(): Boolean {
        if (gcProbe.wasCollected) {
            gcProbe = GCProbe(GC_PRESSURE_SIZE)
            return true
        }

        return false
    }


    override fun start() {
        monitorThread.name = THREAD_NAME
        monitorThread.start()
    }

    override fun stop() {
        synchronized(stopFlagMonitor) {
            stopFlag = true
            try {
                monitorThread.interrupt()
            } catch (t: Throwable) {
                Log.w(LOG_TAG, "Exception during stop...", t)
            }
        }
    }

}