package com.quickbirdstudios.quickboot.monitor

import android.arch.lifecycle.Lifecycle
import android.arch.lifecycle.LifecycleOwner
import java.lang.ref.WeakReference

internal class MonitorReference(lifecycleOwner: LifecycleOwner) {
    private val reference = WeakReference(lifecycleOwner)
    val potentialLeak: Boolean
        get() {
            return reference.get()?.lifecycle?.currentState == Lifecycle.State.DESTROYED
        }

    fun get(): LifecycleOwner? = reference.get()

}