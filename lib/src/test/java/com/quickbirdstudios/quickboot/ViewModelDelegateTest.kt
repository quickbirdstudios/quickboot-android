package com.quickbirdstudios.quickboot

import android.arch.lifecycle.Lifecycle
import android.arch.lifecycle.LifecycleOwner
import android.arch.lifecycle.LifecycleRegistry
import com.quickbirdstudios.quickboot.architecture.lazyViewModel
import org.junit.Assert.*
import org.junit.Test

class ViewModelDelegateTest {

    class Component : LifecycleOwner {
        val registry = LifecycleRegistry(this)
        override fun getLifecycle(): Lifecycle = registry
    }

    @Test
    fun isInitializedOnCreate() {
        val component = Component()
        val delegate = component.lazyViewModel { Any() }
        assertFalse(delegate.isInitialized())

        component.registry.markState(Lifecycle.State.CREATED)
        assertTrue(delegate.isInitialized())
    }

    @Test
    fun containsSameValue() {
        val component = Component()
        val delegate = component.lazyViewModel { Any() }
        val value1 = delegate.value
        component.registry.markState(Lifecycle.State.RESUMED)
        assertSame(value1, delegate.value)
    }
}